\chapter[Derecho de autor cero: entre el derecho de autor y el dominio público]{Derecho de autor cero: \\ entre el derecho de autor y \\ el dominio público}
\chaptermark{Derecho de autor cero}

\noindent La siguiente propuesta surge ante la falta de certeza jurídica
\emph{post mortem} de las obras abiertas, desde el fallecimiento del
autor hasta su traslado al dominio público. Se pretende explicar una
alternativa legal para mediar entre el carácter conservador de las
actuales legislaciones del derecho de autor y el deseo de muchos autores
de que sus obras estén disponibles para todo el público.

Esta propuesta no busca rivalizar con las licencias de uso u otras
alternativas del quehacer cultural en pos de la apertura de la
información. En su lugar, busca complementarlas al centrarse en el
problema de la protección que reciben las obras una vez que los autores
han fallecido.

\section*{Problema}

\subsection*{Antes de la muerte del autor}

\noindent En la actualidad, los autores que no están de acuerdo con la legislación
vigente de derecho de autor cuentan con la alternativa de emplear
licencias de uso. Estas licencias surgieron en el ámbito del
\emph{software} libre y de código abierto, buscando promover la libertad
de uso del material creado por uno o más autores. Por este motivo la
noción de \emph{software} libre ha sido el punto de partida para otros
movimientos como la «cultura libre» o el «acceso abierto». Dentro del
campo que abarca el derecho de autor ---obras literarias o artísticas;
programas de radio o televisión, y \emph{software}, entre otros---
existe una gran cantidad de propuestas de licencias de uso, tales como
las licencias \href{https://creativecommons.org/}{Creative Commons}
(CC), la Licencia de Producción de Pares o la
\href{http://leal.perrotriste.io/}{Licencia Editorial Abierta y Libre}
(LEAL).

Entre los distintos tipos de licencias el concepto de «libertad» es
variable, en algunas ocasiones ambiguo e incluso conflictivo. No todas
las licencias de uso cumplen las mismas necesidades y en ocasiones
\emph{restringen} ciertos usos, resultando contraproducentes para la
libertad en la utilización de una obra. Cabe mencionar que una
característica básica de las licencias de uso es que permiten el empleo
«gratuito» del contenido. En este sentido es preciso indicar que la
gratuidad no necesariamente significa libertad ---el
\href{https://es.wikipedia.org/wiki/Copyleft}{\emph{copyleft}} sirve
para poder distinguir entre una y otra---.

Sin embargo, la mayoría de los autores optan por las licencias de uso a
partir de otras cuestiones:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  comodidad, ya que el trámite es tan sencillo como llenar un formulario
  en internet ---y a veces ni eso---, en contraste con los procesos
  administrativos necesarios para el registro tradicional de una obra;
\item
  economía, porque el registro tradicional por lo general requiere
  cuotas de recuperación, y
\item
  difusión, al no haber necesidad de pedir permiso o pagar por el uso de
  una obra, el usuario será más proclive a difundir el trabajo del
  autor.
\end{enumerate}

\noindent La relación establecida por las licencias de uso entre el autor y el
usuario cambia la dinámica cultural. Ante este reto, diversas
legislaciones han sido modificadas para que semejantes licencias tengan
cabida dentro el ámbito jurídico. Como lo ha indicado CC de manera
clara, no existe una rivalidad entre las licencias y el derecho de
autor, sino una apertura de «todos los derechos reservados» a «algunos
derechos reservados».

Esto ha ayudado a evidenciar un fenómeno recurrente en la producción
cultural: el autor tiene que luchar constantemente para hacer valer su
voluntad ---ya sea de apertura o de cierre---. Aunque el derecho de
autor fue constituido precisamente para salvaguardar los intereses del
autor, este se ve obligado en el día a día a tolerar ciertas prácticas
para tener acceso a una infraestructura cultural en pos de la «libertad»
creativa, de la calidad de su obra o de una difusión más amplia.

Muchos de los autores que desean la apertura de su obra tienen que
enfrentarse al hecho de que la mayoría de las convocatorias o becas para
la producción y difusión de su trabajo implican cierta pérdida de
libertad por parte de los usuarios. Es por ello que en ocasiones se
recurre a alternativas para sustentar la producción abierta de
contenidos, tales como trabajar una jornada adicional, los donativos o
el micromecenazgo.

No obstante, esta tensión supone que el autor está vivo. En el plano
legislativo esto se conoce como actos jurídicos \emph{inter vivos}. Es
decir, el autor tiene al menos la posibilidad de establecer relaciones
que busquen concretar sus intereses. Pero ¿qué pasa cuando el autor
fallece? En el caso habitual los herederos continúan con el control de
los derechos patrimoniales de la obra, «control» que en muchas ocasiones
también fue voluntad del autor. Sin embargo, ¿qué sucede cuando la
voluntad del autor es la apertura de su obra?

\subsection*{Después de la muerte del autor}

\noindent \emph{Las licencias de uso no sustituyen el derecho de autor}, sino que
facilitan su reproducción. Por ello, el derecho de autor continúa
vigente a pesar del empleo de licencias y, también, después de la muerte
del autor.

Por \emph{convención} el fallecimiento del autor no implica el traslado
inmediato de su obra al dominio público. Bajo el argumento de
salvaguardar los intereses de los herederos o de los
productores-editores, hay un lapso de al menos cincuenta años
---establecido por el Convenio de Berna--- para que la obra al fin sea
de acceso y uso libre. No obstante, la prolongación del derecho de autor
suele ser mayor; por ejemplo, en la Unión Europea es de setenta años y
en México de cien años.

Debido a que muchos de los autores mueren intestados, o sus personas
cercanas desconocen los actos jurídicos que fueron celebrados en vida,
se crea una ambigüedad jurídica que dura de cincuenta a cien años. Este
lapso de tiempo impide la oportuna reproducción de la obra, generando lo
que se conoce como «obras huérfanas» ---creaciones cuyos titulares de
derechos no son conocidos o localizables---.

Solo los grandes consorcios tienen la capacidad legal de producir-editar
obras con vacíos jurídicos. Para la mayoría de los productores-editores
independientes, la apuesta por la reproducción de una obra huérfana
implica el riesgo de enfrentarse a situaciones legales que sobrepasan
sus alcances económicos. En estos casos, las obras huérfanas que no
representan altas ganancias ---como pueden ser las obras de culto o para
un público muy específico--- quedan sin ser publicadas durante varios
años.

Pero esta no es la única dificultad: en muchos casos la voluntad del
autor no es respetada por sus herederos. No son pocas las situaciones en
que estos ---sea una institución o algún familiar--- son más reservados
que su testador al momento de establecer acuerdos para publicar. Este
carácter conservador llega al grado de no permitir la reedición de la
obra o mantenerla inédita debido a que los herederos intentan obtener el
mayor beneficio económico, aumentando los costos de producción
---conflicto alimentado por las abusivas prácticas editoriales--- y en
detrimento de la conservación de la herencia cultural.

Para el caso de los autores que en vida expresaron el deseo de apertura
de su obra, este problema se agudiza. También por \emph{convención} el
dominio público no es una adopción voluntaria, sino restringida a las
pautas de tiempo mencionadas con anterioridad. Por ello, en muchas
legislaciones no existe un medio jurídico para poder trasladar de
inmediato una obra al dominio público, aunque la voluntad del autor
hubiera sido la apertura de su obra.

La implementación del dominio público voluntario es un debate abierto
que seguramente permanecerá así durante más tiempo, ya que en varias
legislaciones implicaría una reforma profunda a las leyes del derecho de
autor. Esto no implica que tal posibilidad carezca de importancia, sino
que mientras la discusión continúe existe la necesidad de crear
alternativas, a corto o mediano plazo, para los autores que tienen la
disposición de abrir su obra a todo el público.

Estas alternativas no solo han de enfocarse en la creación de licencias
uso, ya que el problema no se limita al anhelo de algunos autores por el
dominio público voluntario; tampoco a la búsqueda de mayor flexibilidad
en los derechos reservados, dentro de un contexto jurídico que
continuamente está fortaleciendo al derecho de autor. La dificultad
también reside en la concreción de este deseo tras la muerte del autor.

En muchas legislaciones, las licencias de uso tienen un carácter
temporal. El permiso de uso o difusión de la obra no es perpetuo: se
encuentra limitado a cierto tiempo que, de no ser explícito, cada
legislación contempla por defecto. Esto implica que los herederos de los
derechos pueden adquirir de nuevo todos los derechos reservados del
testador. Como se mencionó antes, las licencias no sustituyen al derecho
de autor; esto se traduce en que \emph{las licencias de uso no son
suficientes para mantener la apertura de una obra por tiempo
indefinido}.

No se debe pasar por alto que, ante esta situación, también existen
alternativas que apuestan por un quehacer cultural más democrático,
transparente y comunitario. Sin embargo, mientras el ámbito legislativo
no fomente o proteja este tipo de prácticas, cualquier iniciativa de
apertura de la información que no incida en la capa legal de la
infraestructura cultural carece de garantía para el futuro.

Nuevamente: no por ello carecen de importancia estas iniciativas; por el
contrario, son relevantes para mostrar otras posibilidades de quehacer
cultural, aunque por ahora sean insuficientes para resolver el problema
jurídico de hacer valer la voluntad de apertura expresada por un autor
fallecido.

\section*{Propuesta}

\subsection*{Entidad de gestión}

\noindent \emph{Para salvaguardar el deseo de los autores de mantener abierta su
obra una vez que hayan fallecido, es necesaria una red de organizaciones
o instituciones que velen por este interés}. Estas instituciones han de
recibir obras de manera voluntaria, quedando supeditadas a los autores
que coincidan en la importancia de la libertad de las personas usuarias
para la difusión de su obra y la conservación de la herencia cultural.

Para el eficaz funcionamiento de estas organizaciones se ha de partir
desde una dimensión nacional. El Convenio de Berna solo brinda pautas
mínimas para la gestión internacional del derecho de autor, provocando
que cada legislación presente diferencias sutiles al respecto. Ante tal
situación, es necesario que estas instituciones partan del contexto
jurídico de su Estado correspondiente, para que cada una se constituya
como una entidad de gestión de derechos de autor ---«sociedad de gestión
colectiva» para el caso mexicano---.

Esto no implica que las distintas instituciones nacionales no puedan
organizarse para fomentar lazos de cooperación. Por el contrario, estas
relaciones internacionales son necesarias, ya que las problemáticas
desatadas por el derecho de autor ---y la propiedad intelectual en
general--- no se acotan a un marco nacional. Lo importante es que su
surgimiento sea a partir de las \emph{reglas de juego} de cada nación.

Para evitar los problemas recurrentes dentro de las políticas
culturales, estas entidades han de ser públicas, descentralizadas,
autónomas y con un carácter civil. Además, para facilitar la apertura de
la obra, el trabajo administrativo del día a día ha de realizarse en la
medida de lo posible con formatos abiertos y \emph{software} libre o de
código abierto ---medidas características de las «instituciones
abiertas»---. Asimismo, requerirá el apoyo de otras organizaciones para
el asesoramiento en la conservación de obras culturales libres y
estandarizadas, como puede ser el seguimiento a las pautas indicadas en
la Definición de Obras Culturales Libres
(\href{https://freedomdefined.org/}{Definition of Free Cultural Works}).

Esta red de organizaciones ha de tener como objetivos en común:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  La difusión abierta y gratuita de las obras bajo su resguardo, al
  igual que su conservación.
\item
  La implementación de mecanismos digitales sencillos que permitan al
  autor ceder los derechos de su obra.
\item
  La creación de una estructura tan flexible y democrática como sea
  posible.
\item
  La emisión pública y constante de toda la información concerniente a
  la administración.
\item
  El fomento a la investigación interdisciplinaria en materia de derecho
  de autor y propiedad intelectual.
\item
  El desarrollo y la cooperación tecnológica necesarios para la
  consecución de los objetivos previos.
\end{enumerate}

\noindent De esta manera, el autor que desee que su obra permanezca abierta por
tiempo indefinido puede optar por ceder los derechos patrimoniales a
estas instituciones sin fines de lucro. Ahora bien, estas serían las
principales diferencias entre estas organizaciones y las entidades de
gestión tradicionales:

\begin{itemize}
\item
  La gestión entraría en vigor una vez fallecido el autor, para mantener
  la flexibilidad de uso de su obra sin necesidad de pedir autorización.
\item
  El uso de la obra sería gratuito y no necesitaría un permiso por
  escrito ni un aviso a las instituciones.
\item
  Las organizaciones no emprenderían acciones legales, \emph{excepto}
  cuando los derechos morales sean violados.
\end{itemize}

\noindent Debido a la simplificación administrativa, la flexibilidad jurídica y el
desarrollo tecnológico en pos de la facilidad de uso para autores y
usuarios, estas instituciones presentarían diferencias que se sintetizan
con la concepción del «derecho de autor \emph{cero}»:

\begin{itemize}
\item
  cero pagos,
\item
  cero autorizaciones con antelación,
\item
  cero comunicaciones previas,
\item
  cero complejidades administrativas,
\item
  cero dificultades de uso y
\item
  cero medidas legales.
\end{itemize}

\noindent En cuanto a la capa jurídica, estas organizaciones habrían de erigirse
como entidades de gestión de derecho de autor según las pautas indicadas
en cada legislación correspondiente. Al indicar la voluntad del autor de
ceder sus derechos patrimoniales a esta red cuando fallezca ---lo que se
conoce como acto jurídico \emph{mortis causa}---, estas organizaciones
tendrían los derechos de las obras en un plano legal. Sin embargo, en la
práctica estas instituciones no se desempeñarían de modo punitivo, sino
que procurarían la difusión abierta y gratuita de las obras hasta que
estas al fin formen parte del dominio público.

En este sentido, la relevancia de estas entidades sería la evasión del
vacío jurídico existente en la prolongación del derecho de autor hasta
su traslado al dominio público. Del mismo modo, evitaría que la voluntad
de apertura del autor sea ignorada por los posibles herederos, como
pueden ser otras instituciones habituales dentro de la gestión de
derechos e incluso los mismos familiares.

\subsection*{Donativos y regalías}

\noindent A diferencia de las entidades de gestión tradicionales, las
instituciones del derecho de autor cero buscarían ser financiadas
mediante donativos. Diversas organizaciones vinculadas al
\emph{software} libre o de código abierto, a la cultura libre, al acceso
abierto y a la protección de la privacidad de los usuarios han
demostrado que es un modelo económico factible.

No es la intención que los colaboradores de tiempo completo vivan con
los lujos y comodidades a las que están acostumbradas algunas de las
personas pertenecientes a la industria cultural ---ejecutivos de alto
rango o grandes productores-editores de consorcios internacionales, por
ejemplo---. Pero esto tampoco implica que los colaboradores deban vivir
en situación precaria.

La donación de trabajo sería primordial para el correcto funcionamiento
de estas organizaciones. Sin embargo, en la medida de lo posible, se
habrían de buscar modelos que al menos permitan la retribución de una
renta básica para los colaboradores de tiempo completo. De esta manera
se pretendería mejorar sus condiciones de vida y la concentración de
esfuerzos en el mantenimiento de las instituciones.

Los donativos podrían ser otorgados por cualquier organización o
persona, con la explícita indicación de que esto no permitirá influir en
las decisiones tomadas dentro de las instituciones. El \emph{lobbying}
estaría absolutamente prohibido. Una medida para evitar el cabildeo
sería mediante la publicación de todas las personas o entidades
donadoras y los montos concedidos a cada entidad.

Las personas o instituciones que deseen utilizar alguna de las obras
bajo resguardo de las instituciones del derecho de autor cero podrán
tomarlas de manera gratuita, como se mencionó con anterioridad, o
mediante un donativo voluntario. Estas donaciones deberán estar en una
lista independiente para que así el usuario pueda identificar quiénes
han donado para la utilización del material disponible. Incluso habría
que examinar la viabilidad de que se incluya información referente al
contenido producido a partir de las obras utilizadas.

Ahora bien, muchos de los autores también desearán apoyar económicamente
a otros herederos, por lo cual tendrá que existir la infraestructura
adecuada para que esto sea posible. En estos casos, el autor que ceda su
obra a estas organizaciones deberá poder indicar un porcentaje por
concepto de regalías para otras instituciones o personas de su
preferencia. Así, del donativo obtenido por el uso de su obra ---no de
los donativos hechos directamente a estas organizaciones--- se daría el
porcentaje indicado a los herederos correspondientes. Los porcentajes de
regalías no deberían exceder el cincuenta por ciento, para permitir una
relación equitativa entre el mantenimiento de estas instituciones y el
apoyo al resto de los herederos.

Con estas medidas de financiamiento y administración se buscaría un
trato justo entre el autor, los usuarios, las instituciones del derecho
de autor cero y otros posibles herederos. Es primordial que el autor no
interprete que su obra quedaría «secuestrada» por estas organizaciones,
por lo que la modificación del acuerdo celebrado por este y la
institución nacional correspondiente ha de ser lo más breve posible.

\subsection*{Transparencia}

\noindent Debido a la estructura e intenciones de las instituciones del derecho de
autor cero, toda la información referente a su carácter administrativo
deberá estar disponible en línea para cualquier usuario, sin necesidad
de registro. En especial, la manera en que se utiliza el dinero debería
ser información abierta y lo más detallada posible: ingresos, lista de
donaciones directas, lista de donaciones por la utilización de obras,
lista de colaboradores con renta básica, lista de regalías otorgadas,
lista de gastos, etcétera.

Sería ideal también buscar una administración que \emph{libere pronto y
continuamente}. Así como en el ámbito del \emph{software} existe este
esquema de desarrollo, cabría examinar la posibilidad de contar con
organismos administrativos y de transparencia que liberen sus contenidos
de esta manera, para evitar un mal aprovechamiento del tiempo laboral. O
bien, que al menos la información sobre pagos se publique cada semana,
quincena o mes. En este sentido, un gestor de versiones podría ser
conveniente para poder analizar el historial de modificaciones de estos
datos.

\subsection*{Repositorio público}

\noindent Para la conservación, difusión y uso de las obras, cada institución del
derecho de autor cero deberá tener un repositorio público. La búsqueda y
descarga de contenidos se haría a través de un sitio en red conectado
con todas las bases de datos.

Con la finalidad de alcanzar una mayor difusión y descentralización, los
repositorios deberán tener la posibilidad de ser descargados en su
integridad o de crear espejos. También sería interesante analizar la
viabilidad de subir los contenidos a redes P2P, Library Genesis,
Wikimedia Commons o Internet Archive. Aunque parezca descontrolado e
incluso contraproducente ---pues existe la posibilidad de que un tercero
tergiverse la información o utilice las obras para obtener beneficios
económicos---, muchos de los autores que cedan su obra tendrían
conocimiento de antemano sobre las posibilidades y los peligros que
conlleva la descentralización de la información; aunado a esto, también
se buscarían mecanismos que reduzcan las posibilidades mencionadas. Más
que un riesgo, para varios podría representar la oportunidad de llegar a
plataformas de las que ni siquiera tenían conocimiento y que quizá sean
fundamentales para la difusión de su obra.

Los repositorios públicos son tan esenciales que sin ellos la creación
de estas instituciones tendría muy poco o ningún sentido. Uno de los
principales problemas de las entidades de gestión tradicionales es la
reducida accesibilidad a las obras bajo su resguardo, o al menos a los
metadatos de las mismas. Incluso al existir una base de datos para su
consulta, muchas de estas entidades solo permiten la descarga de
contenidos a través de un muro de pago ---a veces tan poco organizado
que implica una comunicación de ida y vuelta a través de correo
electrónico---.

Por este motivo, la infraestructura y la cooperación técnica son
primordiales para el funcionamiento de estas organizaciones. Sin
embargo, esto no implica un gran desafío técnico. En la actualidad la
creación de repositorios públicos, si bien no es una tarea que se deba
subestimar, tampoco es de tal complejidad que requiera de un equipo
grande de programadores.

\subsection*{Difusión e investigación}

\noindent Los repositorios, los espejos y la disposición del contenido en otras
plataformas no son suficientes para la difusión de las obras. En varios
países «en vías de desarrollo», la interacción dentro de comunidades
digitales independientes a las redes sociales en boga representa un uso
minoritario.

Al partir cada organización, desde contextos nacionales específicos, la
creación de eventos presenciales podría ser una vía de comunicación
directa para alcanzar un mayor espectro en cada comunidad. Más que
promover la propia organización, estos eventos deberían plantearse como
un apoyo para visibilizar diversos prejuicios en torno al derecho de
autor y a la propiedad intelectual, mostrando también otras alternativas
o puntos de flexibilidad de los derechos de propiedad intelectual.

La intención de estos eventos sería fomentar la discusión y el debate
público de algo que se considera propio de los ámbitos jurídicos o
especializados ---a saber, la legislación correspondiente a la propiedad
intelectual---, para evidenciar que en realidad se trata de un tema que
concierne a todas las personas y al futuro de la herencia cultural. Por
este motivo, la vinculación con otras organizaciones nacionales con
posiciones similares a las instituciones del derecho de autor cero,
grandes o pequeñas, formales o informales, es necesaria. Estos nexos
también deberían generarse de la manera más equitativa posible. El
derecho de autor cero no viene a quitarle espacio o importancia a otros
movimientos u organizaciones, sino a suplir una carencia específica,
como lo es la falta de certeza jurídica \emph{post mortem} de las obras
abiertas, desde el fallecimiento del autor hasta su traslado al dominio
público.

Para una difusión crítica en torno a los derechos de propiedad
intelectual, es menester un organismo independiente de investigación.
Con «independencia» se entiende que es preciso realizar investigaciones
interdisciplinarias que no dependan de instituciones como la
Organización Mundial de la Propiedad Intelectual, el Banco Mundial, el
Foro Monetario Internacional, el Centro Regional para el Fomento del
Libro en América Latina y el Caribe o las diversas cámaras nacionales de
la industria cultural, por poner algunos ejemplos.

Con el fin de evitar que se parta de previos supuestos ---como la
importancia de la propiedad intelectual para el desarrollo económico---,
es necesario analizar no solo el discurso de las instituciones
mencionadas en el párrafo anterior, sino también de las organizaciones
con las que tienen afinidades e incluso del mismo discurso del derecho
de autor cero, como es el caso la presente propuesta. En este sentido,
se puede decir que estas organizaciones requieren, en la medida de sus
posibilidades, un departamento de investigación que realice una crítica
\textbf{r}ecursiva y \textbf{f}orzosa a todos los actores involucrados
en el debate sobre la propiedad intelectual ---en otros términos, una
metodología de crítica \texttt{-rf}---.

Todos los archivos concernientes a estas investigaciones, y no solo el
documento final, deberán estar en un repositorio abierto para que
cualquiera pueda cotejarlos y analizarlos. Para quienes solo desean los
resultados finales habrán de crearse publicaciones en múltiples formatos
abiertos o populares, como MD, HTML, EPUB, PDF o MOBI.

\section*{Límites}

\subsection*{Acto jurídico \emph{mortis causa}}

\noindent El derecho de autor cero no busca «competir» con las licencias de uso.
Por ello su ejercicio ha de comenzar una vez fallecido el autor. Cuando
esto suceda, en ningún caso las instituciones del derecho de autor cero
habrán de revocar licencias o demás elementos que el autor haya definido
para la difusión de su obra. Únicamente se concentrarán en conservar y
difundir la obra una vez que el autor no se encuentre con vida.

No obstante, valdría la pena discutir la pertinencia de que el ejercicio
de estas organizaciones comience en cuanto el autor decida ceder su
obra. Un problema grave puede ser que no existan los medios pertinentes
para que estas instituciones tengan conocimiento sobre el fallecimiento
de un autor. Por este motivo, tal vez el inicio de funciones debería
ejecutarse cuando el autor otorgue los archivos de sus obras y de manera
paralela a lo que el autor haga para difundir su trabajo. Esta labor no
debería interferir con los deseos del autor en ningún sentido.

En el caso de la herencia de derechos a partir del fallecimiento, se
celebrará alguna especie de testamento o algún acto jurídico
\emph{mortis causa} similar. Esto implica que cada organización nacional
requerirá de colaboradores con licencias de abogacía, pero también
implica que se han de buscar los mecanismos jurídicos más simples,
económicos y eficientes para asegurar esta transmisión de derechos
patrimoniales. Quizá en algunas legislaciones la creación de un
testamento no sea necesaria; no obstante, siempre se tendrá que contar
con un documento legal \emph{ad hoc} para evitar cualquier revés.

Para el posible caso del ejercicio de funciones de las instituciones del
derecho de autor cero durante el tiempo de vida del autor, y para evitar
que el traslado de derechos patrimoniales impida la publicación de la
obra en medios comerciales que tal vez el autor desee, esta organización
podría actuar como un agente por el cual tales medios puedan acceder al
contenido ---como cualquier otro usuario---. En este sentido, la
relación no sería entre el autor y el medio comercial, sino mediada por
el repositorio de la institución.

El único caso en el que este esquema podría resultar conflictivo sería
si el medio comercial deseara la reserva exclusiva de los derechos. No
obstante, es responsabilidad del autor tener conocimiento de que la
apertura de su obra no es compatible con la reserva de todos los
derechos. En caso de surgir tal dificultad, sería posible plantear que
el autor retire la obra de la organización, tal vez con una pequeña
penalización debido a los gastos jurídicos requeridos para la
celebración de acuerdos legales, aunada a la generación de una lista
pública de obras que fueron retiradas por sus autores.

\subsection*{Alternativa, mas no solución definitiva}

\noindent El derecho de autor cero no pretende imponer su agenda a otras
alternativas abiertas llevadas a cabo por algunas organizaciones o
personas. Por este motivo es necesario enfatizar que \emph{el derecho de
autor cero no es una solución definitiva}, sino una alternativa que se
inserta en la capa legal para mediar entre el carácter conservador de
las actuales legislaciones del derecho de autor y el deseo de muchos
autores de que sus obras estén disponibles para todo el público.

Las instituciones del derecho de autor cero caducarían el día en que la
legislación correspondiente tome la decisión de crear mecanismos
jurídicos oportunos para la conservación abierta de las obras, sin la
necesidad de esperar un lapso de tiempo tras la muerte del autor. Es
decir, estas organizaciones cesarían sus actividades cuando las leyes
del Estado al que pertenezcan permitan el dominio público voluntario.

Mientras tanto, el derecho de autor cero habrá de ser no solo una idea o
un concepto, sino una red de instituciones que puedan manejar la
voluntad \emph{post mortem} del autor de mantener su obra abierta con
todos los requerimientos de la ley. Se hace énfasis en la
institucionalización del derecho de autor cero por el simple hecho de
que es la única medida legal y organizada que permite cumplir con sus
objetivos.

Por ello, cuando se habla de instituciones, el paradigma no son las
instituciones gubernamentales, de iniciativa privada o partidistas, sino
instituciones como \href{https://fsf.org/}{Free Software Foundation},
\href{https://www.eff.org/}{Electronic Frontier Foundation},
\href{https://archive.org/index.php}{Internet Archive},
\href{https://www.documentfoundation.org/}{Document Foundation},
\href{https://creativecommons.org/}{Creative Commons},
\href{http://www.budapestopenaccessinitiative.org/}{Budapest Open Access
Initiative}, \href{https://www.wikipedia.org/}{Wikipedia},
\href{https://www.gutenberg.org/}{Project Gutenberg},
\href{http://www.scielo.org/}{SciELO}, e incluso el
\href{http://ezln.org.mx/}{movimiento zapatista} o el
\href{http://www.congresonacionalindigena.org/}{Congreso Nacional
Indígena}. En este sentido, el término «institución» se usa en su
acepción etimológica de «facilidad», «construcción» y «educación».

Si el derecho de autor cero o el dominio público voluntario son posibles
sin la constitución de organizaciones, qué mejor para los autores que
desean la certeza jurídica de que sus obras permanecerán abiertas una
vez que hayan fallecido: el principal propósito de esta propuesta.
